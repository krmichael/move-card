import React from 'react';
import { View, StyleSheet, Animated } from 'react-native';
import { PanGestureHandler, State } from 'react-native-gesture-handler';

// @refresh reset
export default function CardMove() {
  let offset = 0;
  const translateY = new Animated.Value(0);

  const animatedEvent = Animated.event(
    [
      {
        nativeEvent: {
          translationY: translateY,
        },
      },
    ],
    { useNativeDriver: true },
  );

  function onHandlerStateChanged(event) {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      let opened = false;
      const { translationY } = event.nativeEvent;

      offset += translationY;

      if (translationY >= 100) {
        opened = true;
      } else {
        translateY.setValue(offset);
        translateY.setOffset(0);
        offset = 0;
      }

      Animated.timing(translateY, {
        toValue: opened ? 200 : 0,
        duration: 250,
        useNativeDriver: true,
      }).start(() => {
        offset = opened ? 200 : 0;
        translateY.setOffset(offset);
        translateY.setValue(0);
      });
    }
  }

  const { container, squard } = styles;

  return (
    <View style={container}>
      <PanGestureHandler
        onGestureEvent={animatedEvent}
        onHandlerStateChange={onHandlerStateChanged}>
        <Animated.View
          style={[
            squard,
            {
              transform: [
                {
                  translateY: translateY.interpolate({
                    inputRange: [-200, 0, 200],
                    outputRange: [-50, 0, 200],
                    extrapolate: 'clamp',
                  }),
                },
              ],
            },
          ]}
        />
      </PanGestureHandler>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  squard: {
    width: 300,
    height: 300,
    backgroundColor: '#f2f2f2',
    elevation: 2,
    borderRadius: 10,
  },
});
